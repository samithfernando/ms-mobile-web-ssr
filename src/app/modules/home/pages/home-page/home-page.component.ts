import {AfterViewInit, Component, OnInit} from '@angular/core';
import Typed  from 'typed.js'
import {SpaceService} from "../../../spaces/services/space.service";

@Component({
  selector: 'app-home-page',
  templateUrl: 'home-page.component.html',
  styleUrls: ['home-page.component.scss']
})
export class HomePageComponent implements OnInit, AfterViewInit {

  _toggleMainSearch = false;
  msReviews$;

  constructor(private spaceService: SpaceService) { }

  ngOnInit() {
    this.msReviews$ =  this.spaceService.getMSReviews();

  }

  ngAfterViewInit() {
    /*new Siema({
      selector: '.x',
      duration: 200,
      easing: 'ease-out',
      perPage: 1,
      startIndex: 0,
      draggable: true,
      multipleDrag: true,
      threshold: 20,
      loop: false,
      rtl: false,
    });*/
    new Typed('.event-type', {
      strings: ["Meeting.", "Party.", "Wedding."],
      typeSpeed: 30,
      backDelay: 1000,
      loop: true
    });
  }

  toggleMainSearch() {
    this._toggleMainSearch = !this._toggleMainSearch;
  }

}
