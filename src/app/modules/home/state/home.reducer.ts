import { createFeatureSelector, createSelector } from "@ngrx/store";

export interface HomeState {
  searchKey: string;
}

const initialState: HomeState = {
  searchKey: ''
}

// creating selectors
const getHomeFeatureState = createFeatureSelector<HomeState>('home');

export const getSearchKey = createSelector(
  getHomeFeatureState,
  state => state.searchKey
)

export function homeReducer(state = initialState, action): HomeState {

  switch (action.type) {

    case 'SEARCH_KEY':
      return {
        ...state,
        searchKey: action.payload
      }

    default:
      return state;
  }
}
