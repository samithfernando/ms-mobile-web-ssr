import {Component, AfterViewInit, Input, Output, EventEmitter, OnInit, OnDestroy} from '@angular/core';
import jQuery from 'jquery';
import { PlatformLocation } from "@angular/common";
import { Store, select } from "@ngrx/store";
import { Router } from "@angular/router";
import {SpaceService} from "../../../spaces/services/space.service";
import {getSearchKey} from "../../state/home.reducer";
import * as fromRoot from '../../../../modules/spaces/state/spaces.reducer';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.scss'],
})
export class MainSearchComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('displayMainSearch') displayMainSearch;
  @Output('toggleMainSearch') toggleMainSearch = new EventEmitter<boolean>();

  searchKey: string;

  constructor(
    private location: PlatformLocation,
    private store: Store<fromRoot.State>,
    private router: Router,
    private spaceService: SpaceService
  ) {
    location.onPopState(() => this.closeMainSearch());
  }

  ngOnInit() {

    // TODO: unsubscribe
    this.store.pipe(select(getSearchKey)).subscribe(
      searchKey => this.searchKey = searchKey
    )
  }

  ngAfterViewInit() {
    jQuery('html').addClass('hidden-scroll');
    jQuery('body').addClass('hidden-scroll');
  }

  ngOnDestroy() {
    jQuery('html').removeClass('hidden-scroll');
    jQuery('body').removeClass('hidden-scroll');
  }

  closeMainSearch() {
    jQuery('html').removeClass('hidden-scroll');
    jQuery('body').removeClass('hidden-scroll');
    this.toggleMainSearch.emit(false);
  }

  search(input) {
    this.store.dispatch({
      type: 'SEARCH_KEY',
      payload: input.search
    });


    this.spaceService.getSpaces(input.search).subscribe(response => {
      console.log('response', response);
      this.store.dispatch({
        type: 'SPACE_LIST',
        payload: response.spaces
      })
      this.closeMainSearch();
      this.router.navigate(['/spaces'])
    })




  }

  filterByEventType(eventId: number) {

  }



}
