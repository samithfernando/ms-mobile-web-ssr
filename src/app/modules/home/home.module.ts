import { NgModule } from '@angular/core';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MainSearchComponent } from './components/main-search/main-search.component';
import {SharedModule} from "../../shared/shared.module";
import {StoreModule} from "@ngrx/store";
import {homeReducer} from "./state/home.reducer";

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('home', homeReducer)
  ],
  declarations: [HomePageComponent, MainSearchComponent]
})
export class HomeModule { }
