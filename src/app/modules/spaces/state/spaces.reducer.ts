import * as fromRoot from '../../../state/app.state';

export interface State extends fromRoot.ApplicationState {
  spaces: SpacesState
}

export interface SpacesState {
  spacesList: Array<any>;
}

// set initial state
const initialState: SpacesState = {
  spacesList: []
}

export function spacesReducer (state = initialState, action): SpacesState {
  switch (action.type) {

    case 'SPACE_LIST':
      return {
        ...state,
        spacesList: action.payload
      }

    default:
      return state;
  }
}
