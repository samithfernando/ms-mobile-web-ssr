import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiService} from "../../../core/services/api.service";

@Injectable({
  providedIn: 'root'
})
export class SpaceService {

  private readonly apiRoot = 'https://qapi.millionspaces.com/api'

  constructor(private http: HttpClient, private api: ApiService) { }

  getMSReviews(): Observable<any> {
    const url = `${this.apiRoot}/mobile/reviews`;
    return this.http.get<any>(url);
  }

  getSpaces(key: string) {
    const requestBody = {
      amenities: null,
      available: null,
      budget: null,
      currentPage: 0,
      events: null,
      location: null,
      participation: null,
      rules: null,
      seatingArrangements: null,
      sortBy: null,
      spaceType: [],
      organizationName: key
    }
    return this.api.post(`/space/asearch/page/0`, requestBody)

  }

}
