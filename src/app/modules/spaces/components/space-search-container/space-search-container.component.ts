import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-space-search-container',
  templateUrl: './space-search-container.component.html',
  styleUrls: ['./space-search-container.component.scss']
})
export class SpaceSearchContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
