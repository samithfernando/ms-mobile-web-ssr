import {Component, OnInit, AfterViewInit} from '@angular/core';
import bulmaCarousel from 'bulma-extensions/bulma-carousel/dist/js/bulma-carousel.js'

@Component({
  selector: 'space-img-carousel',
  templateUrl: './img-carousel.component.html',
  styleUrls: ['./img-carousel.component.scss']
})
export class ImgCarouselComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    var carousels = bulmaCarousel.attach();
  }

}
