import { NgModule } from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {SpaceSearchContainerComponent} from "./components/space-search-container/space-search-container.component";
import {SpaceProfileComponent} from "./components/space-profile/space-profile.component";
import {RouterModule} from "@angular/router";
import {SpaceFilterComponent} from "./components/space-filter/space-filter.component";
import {SpaceListComponent} from "./components/space-list/space-list.component";
import {SpaceListItemComponent} from "./components/space-list-item/space-list-item.component";
import {ImgCarouselComponent} from "./components/space-profile/img-carousel/img-carousel.component";
import {StoreModule} from "@ngrx/store";
import {spacesReducer} from "./state/spaces.reducer";

const SPACES_ROUTES = [
  {
    path: '',
    component: SpaceSearchContainerComponent
  },
  {
    path: ':id',
    component: SpaceProfileComponent
  }
]


@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('spaces', spacesReducer),
    RouterModule.forChild(SPACES_ROUTES)
  ],
  declarations: [SpaceSearchContainerComponent, SpaceFilterComponent, SpaceListComponent, SpaceListItemComponent, SpaceProfileComponent, ImgCarouselComponent]
})
export class SpacesModule { }
