import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import jQuery from 'jquery';

@Component({
  selector: 'app-ms-spinner',
  template: `
    <div class="loader-content">
      <div class="loader"></div>
    </div>
  `,
  styles: [`
    .loader-content {
      position: fixed;
      background-color: rgba(255,255,255);
      width: 100%;
      height: 100%;
      z-index: 5;
      top: 0;
      left: 0;
    }

    .loader {
      position: relative;
      margin: auto;
      width: 50px;
      height: 50px;
      top: 45%;
      border: 8px solid #f3f3f3;
      border-radius: 50%;
      border-top: 8px solid #3498db;
      -webkit-animation: spin 1s linear infinite; /* Safari */
      animation: spin 1s linear infinite;
    }
    
    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }
    
    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  `]
})
export class MsSpinnerComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    jQuery('html').addClass('hidden-scroll');
    jQuery('body').addClass('hidden-scroll');
  }

  ngOnDestroy() {
    jQuery('html').removeClass('hidden-scroll');
    jQuery('body').removeClass('hidden-scroll');
  }

}
