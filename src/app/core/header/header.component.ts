import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  whiteLogoPath = '../../../assets/icons/logo-white.svg';
  blueLogoPath = '../../../assets/icons/logo.svg';
  logoPath;

  constructor(public router: Router) { }

  ngOnInit() {
    this.logoPath = this.whiteLogoPath;
  }

  ngAfterViewInit() {
    const navbar = document.getElementById('navbar');
    const navSpans = Array.from(document.querySelectorAll('.nav-span'));
    const sticky = navbar.offsetTop;

    window.onscroll = () => {

      const navbarMenu = document.querySelector('.navbar-menu');
      const navbarBurger = document.querySelector('.navbar-burger');

      if(this.router.url === '/') {
        if (window.pageYOffset > sticky) {
          navbar.classList.add('sticky');
          navSpans.forEach(ele => ele.classList.add('sticky'));
          this.logoPath = this.blueLogoPath;
        } else {
          navbar.classList.remove('sticky');
          navbarMenu.classList.remove('is-active');
          navbarBurger.classList.remove('is-active');
          navSpans.forEach(ele => ele.classList.remove('sticky'));
          this.logoPath = this.whiteLogoPath;
        }
      }
    };
  }

  toggleMenu() {
    const navbarMenu = document.querySelector('.navbar-menu');
    const navbarBurger = document.querySelector('.navbar-burger');
    navbarMenu.classList.toggle('is-active');
    navbarBurger.classList.toggle('is-active')
  }


}
