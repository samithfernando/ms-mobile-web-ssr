import { Component } from '@angular/core';
import {Event, NavigationEnd, NavigationError, NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  isLoading = false;

  constructor(private router: Router) {
    router.events.subscribe( (event: Event) => {

      if (event instanceof NavigationStart) {
        // Show loading indicator
        this.isLoading = true;
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        this.isLoading = false;
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        this.isLoading = false;
      }
    });
  }


}
