import {HomeState} from "../modules/home/state/home.reducer";

export interface ApplicationState {
  home: HomeState
}
